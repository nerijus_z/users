#### Requirements

- PHP 7.1 
- composer
- CURL


#### Getting Started
After clone application go to application folder in terminal and run: 

`composer install`

`./start.sh`

it will: 


- remove old DB
- create new DB
- create DB tables
- start server
- clear cache
- import customer data with CURL through API
    - create 3 groups
    - create 3 users and assign to groups
    
To stop server run `./stop.sh`

     Access server links:

     Admin : http://127.0.0.1:8000
     Api: http://127.0.0.1:8000/api
     
