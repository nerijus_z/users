#!/usr/bin/env bash

cp .env.dev .env


php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create

rm -rf src/Migrations/Version*
php bin/console make:migration
php bin/console doctrine:migrations:migrate -q

php bin/console server:start 127.0.0.1

cp .env.prod .env

php bin/console cache:clear

printf "\n\nCreate Groups wit API \n"

printf "Create Grpup 1\n"
curl -X POST \
  http://127.0.0.1:8000/api/groups \
  -H 'Content-Type: application/json' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
  "title": "Group 1",
  "users": []
}'


printf "\nCreate Group 2\n"
curl -X POST \
  http://127.0.0.1:8000/api/groups \
  -H 'Content-Type: application/json' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
  "title": "Group 2",
  "users": []
}'


printf "\nCreate Gropup 3\n"
curl -X POST \
  http://127.0.0.1:8000/api/groups \
  -H 'Content-Type: application/json' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
  "title": "Group 3",
  "users": []
}'

printf "\n\nCreate User wit API and asign groups\n"

printf "\nCreate User 1\n"
curl -X POST \
  http://127.0.0.1:8000/api/users \
  -H 'Content-Type: application/json' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "name": "User 1",
    "userGroups": [
        "/api/groups/3"
    ]
}'


printf "\nCreate User 2\n"
curl -X POST \
  http://127.0.0.1:8000/api/users \
  -H 'Content-Type: application/json' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "name": "User 2",
    "userGroups": [
        "/api/groups/2",
        "/api/groups/3"
    ]
}'


printf "\nCreate User 3\n"
curl -X POST \
  http://127.0.0.1:8000/api/users \
  -H 'Content-Type: application/json' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "name": "User 3",
    "userGroups": [
        "/api/groups/1",
        "/api/groups/2",
        "/api/groups/3"
    ]
}'

printf "\n"



echo "
===================== All 👍 ===================

      Created by Nerijus Žutautas

      Access server links:

      🌎 Admin : http://127.0.0.1:8000
      📖️ Api: http://127.0.0.1:8000/api

================================================"
